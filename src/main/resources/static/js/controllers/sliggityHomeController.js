sliggityApplication.controller('SliggityHomeController', ['$scope', '$http', 'SliggitySearchService', 'SliggityDateService', function($scope, $http, SliggitySearchService, SliggityDateService) {
    $scope.sliggitySearchResponse = '';

    $scope.heroSearchTypes = SliggitySearchService.getHeroSearchTypes();
    $scope.heroSearchRequest = {};
    $scope.heroSearchRequest.type = $scope.heroSearchTypes[0];

    $scope.heroName = $scope.heroSearchRequest.type.text;

    $scope.modeSearchTypes = SliggitySearchService.getModeSearchTypes();
    $scope.modeSearchRequest = {};
    $scope.modeSearchRequest.type = $scope.modeSearchTypes[0];

    $scope.matchType = $scope.modeSearchRequest.type.text;

    $scope.mapSearchTypes = SliggitySearchService.getMapSearchTypes();
    $scope.mapSearchRequest = {};
    $scope.mapSearchRequest.type = $scope.mapSearchTypes[0];

    $scope.mapName = $scope.mapSearchRequest.type.text;

    $scope.dateSearchTypes = SliggityDateService.getDateSearchTypes();
    $scope.dateSearchRequest = {};
    $scope.dateSearchRequest.type = $scope.dateSearchTypes[0];

    $scope.beginningDate = $scope.dateSearchRequest.type.from;
    $scope.endDate = $scope.dateSearchRequest.type.to;

    $scope.updateHero = function() {
        $scope.heroName = $scope.heroSearchRequest.type.text;
        $scope.search();
    }

    $scope.updateMode = function() {
        $scope.matchType = $scope.modeSearchRequest.type.text;
        $scope.search();
    }

    $scope.updateMap = function() {
        $scope.mapName = $scope.mapSearchRequest.type.text;
        $scope.search();
    }

    $scope.updateDates = function() {
        $scope.beginningDate = $scope.dateSearchRequest.type.from;
        $scope.endDate = $scope.dateSearchRequest.type.to;
        $scope.search();
    }

    $scope.search = function() {
        $scope.heroName = $scope.heroSearchRequest.type.text;
        $scope.matchType = $scope.modeSearchRequest.type.text;
        SliggitySearchService.search($scope.heroName, $scope.beginningDate, $scope.endDate, $scope.matchType, $scope.mapName).then(function(response) {
            $scope.sliggitySearchResponse = response.data.body;
            chartResults($scope.sliggitySearchResponse);
            chartMapResults($scope.sliggitySearchResponse.mapData);
        });
    }

    function chartResults(response) {
        if (response.heroIcon != "All Heroes") {
            $scope.heroIcon = "https://d1i1jxrdh2kvwy.cloudfront.net/Images/Heroes/Portraits/" + response.heroIcon + ".png";
        }

        console.log($scope.sliggitySearchResponse);

        makeChart($scope.sliggitySearchResponse.wins, $scope.sliggitySearchResponse.winLossRatio);
    }

    function chartMapResults(mapData) {
        d3.select(".mapScaleChart").selectAll("*").remove();

        var data = [];
        var text = [];
        var icons = [];

        for (i = 0; i < mapData.length; i++) {
            data.push(mapData[i].winRate);
            text.push(mapData[i].mapName + ": " + mapData[i].wins + " wins / " + mapData[i].matches + " matches = " + mapData[i].winRate + "%");
            icons.push(mapData[i].mapIcon);
        }

        var w = 577;
        var h = 350;

        var color = d3.scaleLinear()
          .domain([d3.min(data), d3.mean(data), d3.max(data)])
          .range(["red", "yellow", "green"]);

        var x = d3.scaleLinear()
                  .domain([0, d3.max(data)])
                  .range([0, w - 28]);

        var chart = d3.select(".mapScaleChart").append("svg")
            .attr("class", "mapChart")
            .attr("width", "100%")
            .attr("height", h+150)
            .append("g")
                .attr("transform", "translate(15,15)");

        var group = chart.selectAll("rect")
            .data(data)
            .enter().append("rect")
            .attr("x", 110)
            .attr("y", function(d, i) {return i * 38;})
            .attr("width", x)
            .style('fill', color)
            .attr("height", 28);

            chart.selectAll("image")
            .data(data)
            .enter().append("image")
            .attr("x", 0)
            .attr("y", function(d, i) { return (i * 38) - 28; })
            .attr("xlink:href", function(d, i) {return "https://d1i1jxrdh2kvwy.cloudfront.net/Images/Maps/" + icons[i] + ".png"})
            .attr("height", 100)
            .attr("width", 100);

        chart.selectAll("text")
            .data(text)
          .enter().append("text")
            .attr("x", 120)
            .attr("y", function(d, i) { return (i * 38) + 14; })
            .attr("dy", ".35em")
            .attr("text-anchor", "start")
            .text(function(d) { return d });
    }

    function makeChart(wins, winLossRatio) {
        d3.select(".chart")
          .style("width", winLossRatio + "%")
          .text(wins);
    }

    $scope.datesAreBlank = function () {
        if($scope.beginningDate == null || $scope.endDate == null || $scope.beginningDate == "" || $scope.endDate == "") {
            return true;
        } else {
            return false;
        }
    }
}]);