sliggityApplication.controller('SliggityProtocolController', ['$scope', '$http', 'SliggitySearchService', function($scope, $http, SliggitySearchService) {
    $scope.sliggitySearchResponse = '';

    $scope.mapSearchTypes = SliggitySearchService.getMapSearchTypes();
    $scope.mapSearchRequest = {};
    $scope.mapSearchRequest.type = $scope.mapSearchTypes[0];

    $scope.mapName = $scope.mapSearchRequest.type.text;

    $scope.updateMap = function() {
        $scope.mapName = $scope.mapSearchRequest.type.text;
        $scope.search();
    }

    $scope.search = function() {
        SliggitySearchService.searchProtocol($scope.mapName).then(function(response) {
            $scope.sliggitySearchResponse = response.data;
            console.log($scope.sliggitySearchResponse);
        });
    }
}]);