package com.hotsliggityface.controllers;

import com.hotsliggityface.helpers.SliggityFaceRequestHelper;
import com.hotsliggityface.services.SliggityFaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class SliggityFaceController {

    @Autowired
    private SliggityFaceService sliggityFaceService;

    @Value("${sliggityService.matches.url}")
    private String matches;

    @GetMapping("/matches")
    public ResponseEntity<?> getMatches(@RequestParam(value = "heroName", required = true) String heroName,
                                                    @RequestParam(value = "beginningDate", required = true) String beginningDate,
                                                    @RequestParam(value = "endDate", required = true) String endDate,
                                                    @RequestParam(value = "matchType", required = true) String matchType,
                                                    @RequestParam(value = "mapName", required = true) String mapName) {

        String matchesUrl = matches
                + "?heroName=" + heroName
                + "&beginningDate=" + beginningDate
                + "&endDate=" + endDate
                + "&matchType=" + matchType
                + "&mapName=" + mapName;

        return ResponseEntity.ok(sliggityFaceService.getMatches(matchesUrl));
    }
}
