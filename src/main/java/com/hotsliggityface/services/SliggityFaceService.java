package com.hotsliggityface.services;

import com.hotsliggityface.helpers.SliggityFaceRequestHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SliggityFaceService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SliggityFaceRequestHelper sliggityFaceRequestHelper;

    public Object getMatches(String matchesUrl) {
        return restTemplate.exchange(matchesUrl, HttpMethod.GET, new HttpEntity<>(sliggityFaceRequestHelper.httpHeaders()), Object.class);
    }
}
