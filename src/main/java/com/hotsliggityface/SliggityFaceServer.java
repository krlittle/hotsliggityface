package com.hotsliggityface;

import org.springframework.boot.builder.SpringApplicationBuilder;

public class SliggityFaceServer extends SliggityFaceApp {
    public static void main(String[] args) {
        new SliggityFaceApp().configure(new SpringApplicationBuilder()).run(args);
    }
}
