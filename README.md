# HotSliggityFace
![#005B00](https://placehold.it/15/005B00/000000?text=+) HotSliggityFace's <code>master</code> branch is working

A Spring Boot application with a user interface made using AngularJS and Bootstrap for making calls to [HotSliggitySlogs](https://github.com/krlittle/HotSliggitySlogs).  

## Purpose
The purpose of this project was to experiment with keeping the UI as a separate component from the backend service.  The idea is that it would be simple to swap out this UI for another one.  Since the RESTful service, [HotSliggitySlogs](https://github.com/krlittle/HotSliggitySlogs), is its own app and not dependent on a specific UI, this gives users the freedom that like [HotSliggitySlogs](https://github.com/krlittle/HotSliggitySlogs) for storing and retrieving their data, but want to design their own interface to it in whatever language they choose.

## How to run
- Make certain you have your MongoDB instance with a database named <code>sliggity</code> running 
- After verifying your MongoDB instance is running, start up your instance of the [HotSliggitySlogs](https://github.com/krlittle/HotSliggitySlogs) app
- Make certain port 8082 is open (or the port you configured in the <code>[application.properties](https://github.com/krlittle/HotSliggityBatch/blob/master/src/main/resources/application.properties)</code> file)
- Run [SliggityFaceServer.java](https://github.com/krlittle/HotSliggityFace/blob/master/src/main/java/com/hotsliggityface/SliggityFaceServer.java) and open your browser to http://localhost:8082
